<h1 align="center"> Projet CNAM Reims </h1>

<h2 align="center">Le 1000 bornes</h2>

---

## Installation 

### Pré-requis: PHP 8

* Récupération des sources
> `git clone https://gitlab.com/Krown_/1000-bornes.git`

## Lancement du jeu
> écrire dans le terminal `php jeu.php`

## Affichage de l'historique de partie
> écrire dans le terminal `php historique.php`