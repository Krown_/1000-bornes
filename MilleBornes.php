<?php

require_once "Plateau.php";
require_once "Player.php";

class MilleBornes
{

    private Plateau $plateau;

    /**
     * @param int $nbPlayers
     * @param string $fileName
     */
    public function __construct(int $nbPlayers, string $fileName)
    {
        if($nbPlayers < 2 && $nbPlayers > 4)
            throw new OutOfRangeException("Impossible de jouer à ".$nbPlayers.", merci de saisir un nombre de joueurs entre 2 et 4");

        $pions = ['⛸','⛟','⛵','⛄'];
        $players = [];

        for($i = 0; $i < $nbPlayers; ++$i)
        {
            $name = readline("Saisir le nom du joueur numéro ".($i+1)." : ");
            $age =  (int)readline("Saisir l'âge du joueur numéro ".($i+1)." : ");

            $res = "";

            for($j = 0, $jMax = count($pions); $j < $jMax; ++$j)
                $res .= $j.":".$pions[$j]."  ";

            $number = (int)readline("Choisir le pion du joueur numéro ".($i+1)." (".$res.") : "); //out of range exception

            $pion = $pions[$number];
            $players[] = new Player($name, $age, $pion);
            array_splice($pions, $number, 1);
        }

        $this->plateau = new Plateau($fileName, $players);

    }

    /**
     * @return Plateau
     */
    public function getPlateau() : Plateau
    {
        return $this->plateau;
    }

    /**
     * @return void
     */
    public function startGame() : void
    {
        $this->plateau->getSabot()->shuffle();
        $this->plateau->distributionCards();
    }

    /**
     * @return bool
     */
    public function gameFinished() : bool
    {
        $players = $this->getPlateau()->getPlayers();
        $res = false;

        if(empty($this->getPlateau()->getSabot()->getPack()))
            $res = true;

        foreach ($players as $player)
            if($player->getPlayArea()->getBornePos() === 1000)
                $res = true;

        return $res;
    }

}