<?php


class Element
{
    private int $score;
    private string $name;
    private int $age;

    public function __construct(int $score, string $name, int $age)
    {
        $this->score = $score;
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    public function __toString() : string
    {
        return $this->score." ".$this->getName()." ".$this->getAge()." ans";
    }

}