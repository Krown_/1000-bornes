<?php

require_once "ArbreBinaire.php";
require_once "Element.php";

$fichier = fopen("historique.txt", "r");
$historique = [];
while (($line = fgets($fichier)) !== false) {
    $infos = explode (",",$line,3);
    $historique[] = new Element($infos[0], $infos[1], str_replace("\r\n","",$infos[2]));
}
fclose($fichier);

$arbre = new ArbreBinaire(null);

foreach ($historique as $element)
{
    $arbre->ajouter($element);
}

echo $arbre;