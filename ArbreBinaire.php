<?php

require_once "Noeud.php";

class ArbreBinaire
{
    private ?Noeud $base;

    public function __construct(?Noeud $base)
    {
        $this->base = $base;
    }

    public function rechercher(int $val) : bool
    {
        return $this->rechercher2($this->base, $val);
    }

    public function rechercher2(Noeud $n, int $val) : bool
    {
        $v = $n->getElement()->getScore();

        if($val > $n)
            return $n->getRightChild() !== null && $this->rechercher2($n->getRightChild(), $val);
        else if($val < $n)
            return $n->getLeftChild() !== null && $this->rechercher2($n->getLeftChild(), $val);
        else
            return true;
    }

    public function getMaximum() : int
    {
        return $this->getMaximum2($this->base);
    }

    public function getMaximum2(Noeud $n) : int
    {
        if($n->getRightChild() === null)
            return $n->getElement()->getScore();

        return $this->getMaximum2($n->getRightChild());
    }

    public function getMinimum() : int
    {
        return $this->getMinimum2($this->base);
    }

    public function getMinimum2(Noeud $n) : int
    {
        if($n->getLeftChild() === null)
            return $n->getElement()->getScore();

        return $this->getMinimum2($n->getLeftChild());
    }

    public function getHauteur() : int
    {
        return $this->getHauteur2($this->base);
    }

    public function getHauteur2(Noeud $n) : int
    {
        if($n === null)
            return 0;

        return 1 + max($this->getHauteur2($n->getLeftChild()), $this->getHauteur2($n->getRightChild()));
    }

    public function getTaille() : int
    {
        return $this->getTaille2($this->base);
    }

    public function getTaille2(Noeud $n) : int
    {
        if($n === null)
            return 0;

        return 1 + $this->getTaille2($n->getLeftChild()) + $this->getTaille2($n->getRightChild());
    }

    public function ajouter(Element $element) : void
    {
        if($this->base === null)
            $this->base = new Noeud($element, null, null);
        else
            $this->ajouter2($this->base, $element);
    }

    public function ajouter2(Noeud $n, Element $element) : Noeud
    {
        if($element->getScore() > $n->getElement()->getScore())
            if($n->getRightChild() === null)
                $n->setRightChild(new Noeud($element, null, null));
            else
                $this->ajouter2($n->getRightChild(), $element);
        else if($element->getScore() <= $n->getElement()->getScore())
            if($n->getLeftChild() === null)
                $n->setLeftChild(new Noeud($element, null, null));
            else
                $this->ajouter2($n->getLeftChild(), $element);

        return $n;
    }

    public function removeNode(Noeud $n, Element $element) : Noeud
    {
        if($n === null)
            $n = new Noeud($element, null, null);

        if($element->getScore() > $n->getElement()->getScore())
            if($n->getRightChild() === null)
                return $n;
            else
                $this->removeNode($n->getRightChild(), $element);
        else if($element->getScore() < $n->getElement()->getScore())
            if($n->getLeftChild() === null)
                return $n;
            else
                $this->removeNode($n->getLeftChild(), $element);

        return $n;
    }

    public function afficherNoeud(?Noeud $n, int $prof) : string
    {
        $res = "";

        for($i = 0; $i < $prof; ++$i)
            $res .= "   ";

        if($n === null)
            $res .= "|->null\n";
        else
        {
            $res .= "|->".$n."\n";
            $res .= $this->afficherNoeud($n->getRightChild(), $prof+1);
            $res .= $this->afficherNoeud($n->getLeftChild(), $prof+1);
        }

        return $res;
    }

    public function __toString() : string
    {
        return $this->afficherNoeud($this->base, 0);
    }



}