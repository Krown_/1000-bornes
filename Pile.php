<?php

require_once "Card.php";
require_once "CardBotte.php";
require_once "CardAttaque.php";
require_once "CardParade.php";
require_once "CardBorne.php";

class Pile
{
    public array $pack;

    /**
     * @param string $nomFichier
     */
    public function __construct(string $nomFichier="")
    {
        $this->pack = array();

        if($nomFichier !== "") {
            if (!file_exists($nomFichier)) {
                echo "Problème d'ouverture du fichier : le tas sera vide\n";
            } else {
                $fichier = fopen($nomFichier, "r");
                while (($line = fgets($fichier)) !== false) {
                    $infos = explode ("-",$line,2);
                    switch ($infos[0])
                    {
                        case 'B':
                            $this->pack[] = new CardBotte(str_replace("\r\n","",$infos[1]));
                            break;
                        case 'A':
                            $this->pack[] = new CardAttaque(str_replace("\r\n","",$infos[1]));
                            break;
                        case 'P':
                            $this->pack[] = new CardParade(str_replace("\r\n","",$infos[1]));
                            break;
                        case 'V':
                            $this->pack[] = new CardBorne(str_replace("\r\n","",$infos[1]));
                            break;
                    }
                }
                fclose($fichier);
            }
        }
    }

    /**
     * @return array
     */
    public function getPack() : array
    {
        return $this->pack;
    }

    /**
     * @param Card $card
     * @return void
     */
    public function addCard(Card $card) : void
    {
        $this->pack[] = $card;
    }

    /**
     * @return void
     */
    public function removeLastCard() : void
    {
        array_splice($this->pack, count($this->pack)-1, 1);
    }

    /**
     * @return void
     */
    public function shuffle() : void
    {
        shuffle($this->pack);
    }
}
