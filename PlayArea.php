<?php

require_once "Pile.php";

class PlayArea
{
    private Pile $speed;
    private Pile $battle;
    private Pile $bornes;
    private Pile $bottes;


    public function __construct()
    {
        $this->speed = new Pile();
        $this->battle = new Pile();
        $this->bornes = new Pile();
        $this->bottes = new Pile();
    }

    /**
     * @param Card $card
     * @throws Exception
     */
    public function addSpeed(Card $card)
    {
        $cardName = $card->getName();
        if($cardName !== "Limite de vitesse" && $cardName !== "Fin de limite de vitesse")
            throw new Exception("Impossible de mettre la carte ".$card->getName()." dans la pile de vitesse");

        $this->speed->addCard($card);
    }

    /**
     * @param Card $card
     * @throws Exception
     */
    public function addBattle(Card $card)
    {
        if(!$card instanceof CardAttaque && !$card instanceof CardParade)
            throw new Exception("Impossible de mettre la carte ".$card->getName()." dans la pile de bataille");

        $this->battle->addCard($card);
    }

    /**
     * @param Card $card
     * @throws Exception
     */
    public function addBorne(Card $card)
    {
        if(!$card instanceof CardBorne)
            throw new Exception("Impossible de mettre la carte ".$card->getName()." dans la pile de bornes");

        $this->bornes->addCard($card);
    }

    /**
     * @param Card $card
     * @throws Exception
     */
    public function addBotte(Card $card, bool $coupFoure)
    {
        if(!$card instanceof CardBotte)
            throw new Exception("Impossible de mettre la carte ".$card->getName()." dans la pile de bottes");

        if($coupFoure)
            $card->setCoupFoure(true);

        $this->bottes->addCard($card);
    }

    /**
     * @return Pile
     */
    public function getBattle(): Pile
    {
        return $this->battle;
    }

    /**
     * @return Pile
     */
    public function getBornes(): Pile
    {
        return $this->bornes;
    }

    /**
     * @return Pile
     */
    public function getBottes(): Pile
    {
        return $this->bottes;
    }

    /**
     * @return bool
     */
    public function hasVehiculePrio() : bool
    {
        $botteCards = $this->getBottes()->getPack();
        $res = false;

        foreach ($botteCards as $botteCard)
        {
            if($botteCard->getName() === "Vehicule prioritaire")
                $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function hasIncrevable() : bool
    {
        $botteCards = $this->getBottes()->getPack();
        $res = false;

        foreach ($botteCards as $botteCard)
        {
            if($botteCard->getName() === "Increvable")
                $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function hasCiterne() : bool
    {
        $botteCards = $this->getBottes()->getPack();
        $res = false;

        foreach ($botteCards as $botteCard)
        {
            if($botteCard->getName() === "Citerne d'essence")
                $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function hasAsVolant() : bool
    {
        $botteCards = $this->getBottes()->getPack();
        $res = false;

        foreach ($botteCards as $botteCard)
        {
            if($botteCard->getName() === "As du volant")
                $res = true;
        }

        return $res;
    }

    /**
     * @return Pile
     */
    public function getSpeed(): Pile
    {
        return $this->speed;
    }

    public function getBornePos() : int
    {
        $bornePack = $this->getBornes()->getPack();

        $totalBorne = 0;

        foreach ($bornePack as $card)
        {
            $totalBorne += $card->getSpeed();
        }

        return $totalBorne;
    }

    /**
     * @param string $packName
     * @return string
     */
    public function nameLastCard(string $packName) : string
    {
        switch ($packName)
        {
            case "speed":
                $cardPack = $this->getSpeed()->getPack();
                $sizePack = count($this->getSpeed()->getPack());
                break;
            case "battle":
                $cardPack = $this->getBattle()->getPack();
                $sizePack = count($this->getBattle()->getPack());
                break;
        }

        if(empty($cardPack))
            $nameLastCard = "Aucune carte";
        else
            $nameLastCard = $cardPack[$sizePack-1]->getName();


        return $nameLastCard;
    }

    /**
     * @param int $cardSpeed
     * @return int
     */
    public function getNbBornesCard(int $cardSpeed) : int
    {
        $cardPack = $this->getBornes()->getPack();
        $res = 0;

        foreach ($cardPack as $card)
        {
            if($card->getSpeed() === $cardSpeed)
                ++$res;
        }

        return $res;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        $cardsName = array("speed", "battle");
        $cardsSpeed = array(25, 50, 75, 100, 200);

        $res = str_repeat(sprintf("   ┌%s┐",str_repeat("-", 26)), 7)."\n";
        $res .= str_repeat(sprintf("%s",str_repeat("   |                          |", 7))."\n",3);

        foreach ($cardsName as $cardName)
        {
            $nameLastCard = $this->nameLastCard($cardName)." ".$cardName;
            $res .= sprintf("   %s", "|".str_repeat(" ", (int)((26 - strlen($nameLastCard)) / 2)).$nameLastCard.str_repeat(" ", (26-strlen($nameLastCard))-(int)((26 - strlen($nameLastCard)) / 2))."|");
        }

        foreach ($cardsSpeed as $cardSpeed)
        {
            $cardBorneName = sprintf("Borne %s : %d", $cardSpeed, $this->getNbBornesCard($cardSpeed));
            $res .= sprintf("   %s", "|".str_repeat(" ", (int)((26 - strlen($cardBorneName)) / 2)).$cardBorneName.str_repeat(" ", (26-strlen($cardBorneName))-(int)((26 - strlen($cardBorneName)) / 2))."|");
        }

        $res .= "\n".str_repeat(sprintf("%s",str_repeat("   |                          |", 7))."\n",3);
        $res .= str_repeat(sprintf("   └%s┘",str_repeat("-", 26)), 7)."\n";

        $bottePack = $this->getBottes()->getPack();

        if(!empty($bottePack))
        {
            $sizeBottePack = count($bottePack);


            $res .= str_repeat(sprintf("   ┌%s┐",str_repeat("-", 26)), $sizeBottePack)."\n";
            $res .= str_repeat(sprintf("%s",str_repeat("   |                          |", $sizeBottePack))."\n",3);

            foreach ($bottePack as $card)
            {
                $cardBotteName = $card->getName();
                $res .= sprintf("   %s", "|".str_repeat(" ", (int)((26 - strlen($cardBotteName)) / 2)).$cardBotteName.str_repeat(" ", (26-strlen($cardBotteName))-(int)((26 - strlen($cardBotteName)) / 2))."|");
            }

            $res .= "\n";

            foreach ($bottePack as $card)
                if($card->hasCoupFoure())
                    $res .= sprintf("%s","   |       Coup fouré !       |");
                else
                    $res .= sprintf("%s","   |                          |");


            $res .= "\n".str_repeat(sprintf("%s",str_repeat("   |                          |", $sizeBottePack))."\n",2);
            $res .= str_repeat(sprintf("   └%s┘",str_repeat("-", 26)), $sizeBottePack)."\n";
        }

        return $res;
    }

}