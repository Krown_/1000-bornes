<?php

require_once "Pile.php";

class Plateau
{

    private Pile $sabot;
    private Pile $defausse;
    private array $players;

    /**
     * @param string $fichier
     * @param array $players
     */
    public function __construct(string $fichier, array $players)
    {
        $this->sabot = new Pile($fichier);
        $this->defausse = new Pile();
        $this->players = $players;
    }

    /**
     * @return Pile
     */
    public function getSabot() : Pile
    {
        return $this->sabot;
    }

    /**
     * @return Pile
     */
    public function getDefausse() : Pile
    {
        return $this->defausse;
    }

    /**
     * @return array
     */
    public function getPlayers() : array
    {
        return $this->players;
    }

    /**
     * @return int
     */
    public function getMaxCharPlayersName() : int
    {
        $names = array();
        foreach ($this->players as $player)
            $names[] = $player->getName();

        return strlen(max($names));
    }

    /**
     * @return void
     */
    public function distributionCards() : void
    {
        for ($i = 0; $i < 6; ++$i)
            foreach ($this->players as $player)
            {
                $pack = $this->getSabot()->getPack();
                $player->addHand($pack[count($pack)-1]);
                $this->getSabot()->removeLastCard();
            }
    }

    public function distributionCard(Player $player) : void
    {
        $pack = $this->getSabot()->getPack();
        $player->addHand($pack[count($pack)-1]);
        $this->getSabot()->removeLastCard();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        $res = sprintf("   %s",str_repeat("-", ($this->getMaxCharPlayersName()+169)))."\n";

        foreach ($this->players as $player)
        {
            $color = !$player->canMove() ? "\033[31m" : ($player->canMoveSlowly() ? "\033[93m" : "\033[92m");
            $res .= sprintf("   | %s|", $player->getName().str_repeat(" ", ($this->getMaxCharPlayersName()-strlen($player->getName())+2)));

            $nbBorne = $player->getPlayArea()->getBornePos();

            if($nbBorne === 0)
            {
                $res .= sprintf(" %s", $color."".$player->getPion()." \033[0m|");
                $res .= sprintf("%s", str_repeat("   |", 40))."\n";
            }
            else
            {
                $res .= sprintf("%s", str_repeat("   |", $nbBorne/25));
                $res .= sprintf(" %s", $color."".$player->getPion()." \033[0m|");
                $res .= sprintf("%s", str_repeat("   |", 40-($nbBorne/25)))."\n";
            }

            $res .= sprintf("   %s",str_repeat("-", ($this->getMaxCharPlayersName()+169)))."\n";
        }

        return $res;
    }
}