<?php

require_once "MilleBornes.php";
require_once "Player.php";

$nbPlayers = 0;

do {
    $nbPlayers = (int)readline("Saisir un nombre de joueur entre 2 et 4 compris : ");
} while ($nbPlayers < 2 || $nbPlayers > 4);

$milleBornes = new MilleBornes($nbPlayers, "cartes.txt");

$milleBornes->startGame();

$plateau = $milleBornes->getPlateau();

$players = $plateau->getPlayers();

$youngerPlayer = 0;

for ($i = 1; $i < $nbPlayers; ++$i)
{
    if(!$players[$youngerPlayer]->isYounger($players[$i]))
        $youngerPlayer = $i;
}

$youngerPlayer = 1;

$playersOrdered = array_slice($players, $youngerPlayer, count($players)-$youngerPlayer+1);

array_splice($players, $youngerPlayer, count($players)-$youngerPlayer+1);

$playersOrdered = array_merge($playersOrdered, $players);

do {

    foreach ($playersOrdered as $player)
    {
        $plateau->distributionCard($player);

        $hand = $player->getHand();
        $nbCard =  count($hand);

        $affichagePlayers = "Liste des joueurs : \n";
        for ($j = 0; $j < $nbPlayers; ++$j)
        {
            if($player !== $playersOrdered[$j])
                $affichagePlayers .= "$j : ".$playersOrdered[$j]->getName()."\n";
        }

        $affichagePlayArea = (bool)readline("Voulez-vous afficher votre zone de jeu ? (oui : 1, non : 0) ");

        if($affichagePlayArea)
            echo "Affichage de votre zone de jeu : \n".$player->getPlayArea();

        do {
            do {
                echo "\033[34mTour de jeu de : \033[92m".$player->getName()."\n\033[0m";
                echo "\033[34mChoisir une action :\n\033[36m1. Choisir une carte\n2. Regarder la zone de jeu d'un joueur\n3. Voir le plateau\n4. Jeter une carte dans la défausse\n\033[0m";
                $action = (int)readline("votre choix (1,2,3,4) : ");
            } while($action < 1 || $action > 4);

            $error = true;

            switch ($action)
            {
                case 1:
                    $retour = false;
                    echo "Affichage de votre main : \n".$player;

                    do {
                        do {
                            $cardNumber = (int)readline("Choisir une carte (pour un retour écrire : -1) : ");
                        } while(($cardNumber < 0 || $cardNumber > $nbCard) && $cardNumber !== -1);

                        if($cardNumber !== -1)
                        {
                            $card = $hand[$cardNumber];

                            $playerArea = $player->getPlayArea();

                            switch (get_class($card))
                            {
                                case "CardBorne":
                                    if($player->canMove())
                                        if($playerArea->getBornePos()+$card->getSpeed() <= 1000)
                                            if(!$player->canMoveSlowly()) {
                                                $canPlay = false;
                                                if($card->getSpeed() === 200) {
                                                    if ($player->getNb200() < 2) {
                                                        $player->addNb200();
                                                        $canPlay = true;
                                                    }
                                                    else
                                                    {
                                                        echo "\033[31mImpossible, d'utiliser plus de deux bornes 200km/h\n\033[0m";
                                                        $error = true;
                                                    }
                                                }
                                                else
                                                    $canPlay = true;

                                                if($canPlay)
                                                {
                                                    $playerArea->addBorne($card);
                                                    $player->removeFromHand($cardNumber);
                                                    $error = false;
                                                    $retour = true;
                                                }

                                            }
                                            else if($card->getSpeed() <= 50) {
                                                $playerArea->addBorne($card);
                                                $player->removeFromHand($cardNumber);
                                                $error = false;
                                                $retour = true;
                                            }
                                            else
                                                echo "\033[31mImpossible, vous ne pouvez vous déplacer à plus de 50km/h\n\033[0m";
                                        else
                                            echo "\033[31mImpossible, vous ne pouvez dépasser les 1000 bornes !\n\033[0m";
                                    else
                                        echo "\033[31mImpossible de bouger, votre véhicule est bloqué !\n\033[0m";
                                    break;
                                case "CardBotte":
                                    if($player->isAttacked())
                                    {
                                        $cardName = $card->getName();

                                        $attaqueCardName = $player->getPlayArea()->nameLastCard("battle");

                                        switch ($cardName)
                                        {
                                            case "Increvable":
                                                if($attaqueCardName === "Crevaison")
                                                    $player->setIsAttacked(false);
                                                break;
                                            case "Citerne d'essence":
                                                if($attaqueCardName === "Panne d'essence")
                                                    $player->setIsAttacked(false);
                                                break;
                                            case "Vehicule prioritaire":
                                                if($attaqueCardName === "Feu rouge")
                                                    $player->setIsAttacked(false);
                                                break;
                                            case "As du volant":
                                                if($attaqueCardName === "Accident")
                                                    $player->setIsAttacked(false);
                                                break;
                                        }
                                    }
                                    $playerArea->addBotte($card, false);
                                    $player->removeFromHand($cardNumber);
                                    $error = false;
                                    $retour = true;
                                    break;
                                case "CardParade":

                                    $cardName = $card->getName();
                                    if($cardName !== "Fin de limite de vitesse")
                                    {
                                        if($player->canStart())
                                        {
                                            if($player->isAttacked())
                                            {
                                                $attaqueCardName = $player->getPlayArea()->nameLastCard("battle");
                                                $isAttacked = false;

                                                switch ($cardName)
                                                {
                                                    case "Feu vert":
                                                        if($attaqueCardName === "Feu rouge" && !$player->getPlayArea()->hasVehiculePrio())
                                                            $isAttacked = true;
                                                        break;
                                                    case "Essence":
                                                        if($attaqueCardName === "Panne d'essence" && !$player->getPlayArea()->hasCiterne())
                                                            $isAttacked = true;
                                                        break;
                                                    case "Roue de secours":
                                                        if($attaqueCardName === "Crevaison" && !$player->getPlayArea()->hasIncrevable())
                                                            $isAttacked = true;
                                                        break;
                                                    case "Reparation":
                                                        if($attaqueCardName === "Accident" && !$player->getPlayArea()->hasAsVolant())
                                                            $isAttacked = true;
                                                        break;
                                                }

                                                if($isAttacked)
                                                {
                                                    $player->getPlayArea()->addBattle($card);
                                                    $player->setIsAttacked(false);
                                                    $player->removeFromHand($cardNumber);
                                                    $error = false;
                                                    $retour = true;
                                                }
                                                else
                                                {
                                                    echo "\033[31mImpossible, vous êtes protégé !\n\033[0m"; //edit
                                                    $error = true;
                                                }
                                            }
                                            else
                                            {
                                                echo "\033[31mImpossible, vous n'êtes pas attaqué !\n\033[0m";
                                                $error = true;
                                            }
                                        }
                                        else
                                        {
                                            if($cardName === "Feu vert")
                                            {
                                                $player->getPlayArea()->addBattle($card);
                                                $player->removeFromHand($cardNumber);
                                                $error = false;
                                                $retour = true;
                                            }

                                        }

                                    }
                                    else
                                    {
                                        if(!$player->getPlayArea()->hasVehiculePrio())
                                        {
                                            if($player->canMoveSlowly())
                                            {
                                                $player->getPlayArea()->addSpeed($card);
                                                $player->removeFromHand($cardNumber);
                                                $error = false;
                                                $retour = true;
                                            }
                                            else
                                            {
                                                echo "\033[31mImpossible, vous n'êtes pas limité !\n\033[0m";
                                                $error = true;
                                            }
                                        }
                                        else
                                        {
                                            echo "\033[34mVous avez un véhicule prioritaire !\n\033[0m";
                                            $error = true;
                                        }

                                    }
                                    break;
                                case "CardAttaque":
                                    echo $affichagePlayers;
                                    do {
                                        $numPlayer = (int)readline("Choisir un joueur ? (numéro) : ");
                                    } while($numPlayer < 0 || $numPlayer > $nbPlayers);

                                    $cardName = $card->getName();
                                    $target = $playersOrdered[$numPlayer];

                                    if($cardName !== "Limite de vitesse")
                                    {
                                        if(!$target->isAttacked())
                                        {
                                            $isProtected = false;

                                            switch ($cardName)
                                            {
                                                case "Feu rouge":
                                                    if($target->getPlayArea()->hasVehiculePrio())
                                                        $isProtected = true;
                                                    break;
                                                case "Panne d'essence":
                                                    if($target->getPlayArea()->hasCiterne())
                                                        $isProtected = true;
                                                    break;
                                                case "Crevaison":
                                                    if($target->getPlayArea()->hasIncrevable())
                                                        $isProtected = true;
                                                    break;
                                                case "Accident":
                                                    if($target->getPlayArea()->hasAsVolant())
                                                        $isProtected = true;
                                                    break;
                                            }

                                            if(!$isProtected)
                                            {
                                                $target->getPlayArea()->addBattle($card);
                                                $target->setIsAttacked(true);
                                                $player->removeFromHand($cardNumber);
                                                $error = false;
                                                $retour = true;
                                            }
                                            else
                                            {
                                                echo "\033[31mImpossible la personne est protégée !\n\033[0m";
                                                $error = true;
                                            }
                                        }
                                        else {
                                            echo "\033[31mImpossible la personne est déjà attaquée !\n\033[0m";
                                            $error = true;
                                        }
                                    }
                                    else
                                        if(!$target->isUnderSpeedLimit())
                                            if(!$target->getPlayArea()->hasVehiculePrio())
                                            {
                                                $target->getPlayArea()->addSpeed($card);
                                                $player->removeFromHand($cardNumber);
                                                $error = false;
                                                $retour = true;
                                            }
                                            else {
                                                echo "\033[31mImpossible la personne possède un véhicule prioritaire !\n\033[0m";
                                                $error = true;
                                            }
                                        else
                                        {
                                            echo "\033[31mImpossible la personne est déjà limitée !\n\033[0m";
                                            $error = true;
                                        }
                                    break;
                            }
                            if($error)
                                $retour = (int)readline("Voulez-vous retourner au menu précédent ? (0 : Non, 1 : Oui) : ");
                        }
                        else
                            $retour = true;

                    } while(!$retour);
                    break;
                case 2:
                    echo $affichagePlayers;
                    do {
                        $numPlayer = (int)readline("Choisir un joueur ? (numéro) : ");
                    } while($numPlayer < 0 || $numPlayer > $nbPlayers);

                    echo "\033[34mAffichage de la zone de jeu de \033[92m".$playersOrdered[$numPlayer]->getName()."\n\033[0m".$playersOrdered[$numPlayer]->getPlayArea();
                    break;
                case 3:
                    echo "\033[34mAffichage du plateau :\n\033[0m".$plateau;
                    break;
                case 4:
                    echo "\033[34mAffichage de votre main : \n\033[0m".$player;
                    do {
                        $cardNumber = (int)readline("Choisir une carte (pour un retour écrire : -1) : ");
                    } while(($cardNumber < 0 || $cardNumber > $nbCard) && $cardNumber !== -1);
                    if($cardNumber !== -1)
                    {
                        $card = $hand[$cardNumber];
                        $plateau->getDefausse()->addCard($card);
                        $player->removeFromHand($cardNumber);
                        $error = false;
                    }
                    break;
            }
        } while($error);

        echo $plateau;

    }

} while(!$milleBornes->gameFinished());

$win = $playersOrdered[0];
$other = "";

$file = fopen("historique.txt","w");

for($i = 0, $placement = 2; $i < $nbPlayers; ++$i)
{
    if($win->getPlayArea()->getBornePos() < $playersOrdered[$i]->getPlayArea()->getBornePos())
        $win = $playersOrdered[$i];

    $other .= " Joueur $placement: " . $playersOrdered[$i]->getName()." avec un score de ".$playersOrdered[$i]->getPlayArea()->getBornePos()."\n";
    ++$placement;
    fwrite($file,$playersOrdered[$i]->getPlayArea()->getBornePos().",".$playersOrdered[$i]->getName().",".$playersOrdered[$i]->getAge()."\r\n");
}
fclose($file);
echo $other."\n";
echo "Le vainqueur est donc ".$win->getName()." avec un score de : ".$win->getPlayArea()->getBornePos()."\n";
