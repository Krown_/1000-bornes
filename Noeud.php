<?php

require_once "Element.php";

class Noeud
{
    private Element $element;
    private ?Noeud $leftChild;
    private ?Noeud $rightChild;

    public function __construct(Element $element, ?Noeud $leftChild, ?Noeud $rightChild)
    {
        $this->element = $element;
        $this->leftChild = $leftChild;
        $this->rightChild = $rightChild;
    }

    /**
     * @return Element
     */
    public function getElement(): Element
    {
        return $this->element;
    }

    /**
     * @param Element $element
     */
    public function setElement(Element $element): void
    {
        $this->element = $element;
    }

    /**
     * @return Noeud|null
     */
    public function getLeftChild(): ?Noeud
    {
        return $this->leftChild;
    }

    /**
     * @param Noeud|null $leftChild
     */
    public function setLeftChild(?Noeud $leftChild): void
    {
        $this->leftChild = $leftChild;
    }

    /**
     * @return Noeud|null
     */
    public function getRightChild(): ?Noeud
    {
        return $this->rightChild;
    }

    /**
     * @param Noeud|null $rightChild
     */
    public function setRightChild(?Noeud $rightChild): void
    {
        $this->rightChild = $rightChild;
    }

    public function __toString() : string
    {
        return $this->element;
    }

}