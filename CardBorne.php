<?php


class CardBorne extends Card
{

    private int $speed;

    /**
     * @param int $speed
     */
    public function __construct(int $speed)
    {
        parent::__construct("Borne");
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getSpeed() : int
    {
        return $this->speed;
    }

}