<?php

require_once "Card.php";

class CardBotte extends Card
{
    private bool $coupFoure;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->coupFoure = false;
    }

    /**
     * @return bool
     */
    public function hasCoupFoure() : bool
    {
        return $this->coupFoure;
    }

    /**
     * @param bool $coupFoure
     */
    public function setCoupFoure(bool $coupFoure)
    {
        $this->coupFoure = $coupFoure;
    }
}