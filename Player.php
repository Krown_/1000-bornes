<?php

require_once "PlayArea.php";

class Player
{
    private string $name;
    private int $age;
    private string $pion;
    private PlayArea $playArea;
    private array $hand = [];
    private bool $isAttacked = false;
    private int $nb200;

    /**
     * @param string $name
     * @param int $age
     * @param string $pion
     */
    public function __construct(string $name, int $age, string $pion)
    {
        $this->name = $name;
        $this->age = $age;
        $this->pion = $pion;
        $this->playArea = new PlayArea();
        $this->nb200 = 0;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getPion(): string
    {
        return $this->pion;
    }

    /**
     * @return PlayArea
     */
    public function getPlayArea() : PlayArea
    {
        return $this->playArea;
    }

    /**
     * @return array
     */
    public function getHand() : array
    {
        return $this->hand;
    }

    /**
     * @return int
     */
    public function getNb200() : int
    {
        return $this->nb200;
    }

    /**
     * @param int $nb200
     */
    public function addNb200() : void
    {
        ++$this->nb200;
    }

    /**
     * @param bool $isAttacked
     */
    public function setIsAttacked(bool $isAttacked): void
    {
        $this->isAttacked = $isAttacked;
    }

    /**
     * @return bool
     */
    public function isAttacked(): bool
    {
        return $this->isAttacked;
    }

    /**
     * @param int $i
     * @return void
     */
    public function removeFromHand(int $i) : void
    {
        array_splice($this->hand, $i, 1);
    }

    /**
     * @param Card $card
     * @return void
     */
    public function addHand(Card $card) : void
    {
        $this->hand[] = $card;
    }

    /**
     * @param Player $player
     * @return bool
     */
    public function isYounger(Player $player) : bool
    {
        return $this->getAge() <= $player->getAge();
    }

    public function isUnderSpeedLimit() : bool
    {
        $cardName = $this->getPlayArea()->nameLastCard("battle");

        return $cardName === "Limite de vitesse";
    }

    /**
     * @return bool
     */
    public function canStart() : bool
    {
        $battleCards = $this->getPlayArea()->getBattle()->getPack();
        $botteCards = $this->getPlayArea()->getBottes()->getPack();
        $res = false;

        if($this->getPlayArea()->hasVehiculePrio())
            $res = true;

        if(!$res)
            foreach ($battleCards as $battleCard)
            {
                if($battleCard->getName() === "Feu vert")
                    $res = true;
            }

        return $res;
    }

    /**
     * @return bool
     */
    public function canMove() : bool
    {
        $battleCards = $this->getPlayArea()->getBattle()->getPack();

        $res = true;

        if($this->canStart())
        {
            $res = !$this->isAttacked();
            /*if (!empty($battleCards))
            {
                $res = match ($battleCards[count($battleCards) - 1]->getName()) {
                    "Feu rouge", "Accident", "Crevaison", "Panne d'essence" => false,
                    default => true,
                };
            }*/
        }
        else {
            $res = false;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function canMoveSlowly() : bool
    {
        $res = false;

        $speedCards = $this->getPlayArea()->getSpeed()->getPack();

        if(!empty($speedCards) && !$this->getPlayArea()->hasVehiculePrio())
            $res = match ($speedCards[count($speedCards) - 1]->getName()) {
                "Limite de vitesse" => true,
                default => false,
            };

        return $res;
    }


    public function __toString() : string
    {
        $hand = $this->getHand();
        $nbCard = count($hand);

        $res = str_repeat(sprintf("   ┌%s┐",str_repeat("-", 26)), $nbCard)."\n";
        $res .= str_repeat(sprintf("%s",str_repeat("   |                          |", $nbCard))."\n",3);

        foreach ($hand as $card)
        {
            if($card instanceof CardBorne)
                $cardName = sprintf("Borne %s ", $card->getSpeed());
            else
                $cardName = $card->getName();
            $res .= sprintf("   %s", "|".str_repeat(" ", (int)((26 - strlen($cardName)) / 2)).$cardName.str_repeat(" ", (26-strlen($cardName))-(int)((26 - strlen($cardName)) / 2))."|");
        }


        $res .= "\n".str_repeat(sprintf("%s",str_repeat("   |                          |", $nbCard))."\n",3);
        $res .= str_repeat(sprintf("   └%s┘",str_repeat("-", 26)), $nbCard)."\n";

        return $res;
    }

}